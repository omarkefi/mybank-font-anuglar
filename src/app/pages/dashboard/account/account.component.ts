import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ClientServiceService } from 'src/app/shared/services/client-service.service';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss']
})
export class AccountComponent implements OnInit {
  selectedAccount: any;
  constructor(private ClientService: ClientServiceService, private routeSnapshot: ActivatedRoute) { }

  ngOnInit(): void {
    this.routeSnapshot.paramMap.subscribe((params) => {
      this.selectedAccount = this.ClientService.currentUser.accounts.filter(account => account.accountNumber == params.get("numCmpt")?.toString())[0];
      console.log(params.get("numCmpt"))
      console.log(this.selectedAccount)
    })
  }

}
