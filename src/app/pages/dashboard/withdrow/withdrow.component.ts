import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ClientServiceService } from 'src/app/shared/services/client-service.service';

@Component({
  selector: 'app-withdrow',
  templateUrl: './withdrow.component.html',
  styleUrls: ['./withdrow.component.scss']
})
export class WithdrowComponent implements OnInit {
  withdrowF!: FormGroup;
  constructor(private ClientService: ClientServiceService, private routeSnapshot: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    this.withdrowF = new FormGroup({
      montant: new FormControl('', [Validators.required])
    })
  }
  withdrow() {
    this.routeSnapshot.paramMap.subscribe((params) => {
      this.ClientService.withdrow(params.get("numCmpt"), this.withdrowF.get('montant')?.value);
      setTimeout(() => {
        this.router.navigateByUrl("/");
      }, 1000);
    })
  }
}
