import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import { AccountComponent } from './account/account.component';
import { WithdrowComponent } from './withdrow/withdrow.component';

const routes: Routes = [
  {
    path: '', children: [{
      path: '', component: DashboardComponent
    },
    {
      path: 'account/:numCmpt', component: AccountComponent
    }, {
      path: 'withdrow/:numCmpt', component: WithdrowComponent
    }]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
