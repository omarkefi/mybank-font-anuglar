import { Component, OnInit } from '@angular/core';
import { ClientServiceService } from 'src/app/shared/services/client-service.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  connecteduser = this.ClientService.currentUser;
  constructor(private ClientService: ClientServiceService) { }

  ngOnInit(): void {
  }

}
