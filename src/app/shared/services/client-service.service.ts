import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ClientServiceService {
  currentUser = {
    firstname: 'omar',
    lastname: 'kefi',
    accounts: [
      {
        accountNumber: "HFAX130E392",
        balance: 200,
        seuil: 500,
        operations: [{
          date: "05/04/2023",
          montant: 500,
          type: "Retrait",
          parQui: "omar kefi",
          solde: 200
        },
        {
          date: "04/04/2023",
          montant: 200,
          type: "DEPOT",
          parQui: "omar kefi",
          solde: 700
        },
        {
          date: "03/04/2023",
          montant: 500,
          type: "Retrait",
          parQui: "omar kefi",
          solde: 500
        },
        {
          date: "02/04/2023",
          montant: 1000,
          type: "DEPOT",
          parQui: "omar kefi",
          solde: 1000
        },]
      }, {
        accountNumber: "HFAX145092",
        balance: -100,
        seuil: 200,
        operations: [{
          date: "05/04/2023",
          montant: 1400,
          type: "Retrait",
          parQui: "omar kefi",
          solde: -100
        },
        {
          date: "04/04/2023",
          montant: 300,
          type: "DEPOT",
          parQui: "omar kefi",
          solde: 1300
        },
        {
          date: "03/04/2023",
          montant: 1000,
          type: "Retrait",
          parQui: "omar kefi",
          solde: 1000
        },
        {
          date: "02/04/2023",
          montant: 2000,
          type: "DEPOT",
          parQui: "omar kefi",
          solde: 2000
        },]
      }]
  }
  constructor() { }

  withdrow(accountNumber: any, amount: any) {
    let account = this.currentUser.accounts.filter(account => account.accountNumber == accountNumber)[0];
    let newBalance = account.balance - amount;
    let operation = {
      date: "06/04/2023",
      montant: amount,
      parQui: "omarkefi",
      type: "Retrait",
      solde: newBalance
    }
    account.balance = newBalance;
    account.operations.push(operation);

  }
  deposit(accountNumber: any, amount: any) {
    let account = this.currentUser.accounts.filter(account => account.accountNumber == accountNumber)[0];
    let newBalance = account.balance + amount;
    let operation = {
      date: "06/04/2023",
      montant: amount,
      parQui: "omarkefi",
      type: "Retrait",
      solde: newBalance
    }
    account.balance = newBalance;
    account.operations.push(operation);

  }
}
