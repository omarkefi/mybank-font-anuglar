import { Component, OnInit } from '@angular/core';
import { ClientServiceService } from 'src/app/shared/services/client-service.service';

@Component({
  selector: 'app-client-navbar',
  templateUrl: './client-navbar.component.html',
  styleUrls: ['./client-navbar.component.scss']
})
export class ClientNavbarComponent implements OnInit {
  connecteduser = this.ClientService.currentUser;
  constructor(private ClientService: ClientServiceService) { }

  ngOnInit(): void {
  }

}
