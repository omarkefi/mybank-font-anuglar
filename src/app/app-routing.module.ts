import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ClientlayoutComponent } from './layout/clientlayout/clientlayout.component';

const routes: Routes = [
  {
    path: 'dashboard', component: ClientlayoutComponent, loadChildren: () => import('./pages/dashboard/dashboard.module').then(m => m.DashboardModule)
  },
  { path: '**', redirectTo: 'dashboard' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
